package com.cqwo.gamewaiter.framework.activity;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.cqwo.gamewaiter.R;
import com.cqwo.gamewaiter.core.helper.CWMUtils;
import com.cqwo.gamewaiter.framework.service.UMengPushService;
import com.orm.SugarContext;

/**
 * Created by cqnews on 2018/3/8.
 */

public class BaseApplication extends Application {


    static Context baseApplicationContext;

    /**
     * 上下文线程,供voller框架使用
     */
    public static RequestQueue requestQueue;


    public static SharedPreferences sp;



    public static boolean isConnectBluetoothSuccess = false;


    @Override
    public void onCreate() {

        super.onCreate ();

        baseApplicationContext = getBaseContext ();


        sp = getSharedPreferences ("gamewater", Activity.MODE_PRIVATE);

        CWMUtils.init (this);


        initService ();


        /**
         * Volley线程驻入
         */
        requestQueue = Volley.newRequestQueue (this);

        /**
         * 数据库注入
         */
        SugarContext.init (this);


    }

    public void onTerminate() {

        super.onTerminate ();

        SugarContext.terminate ();
    }


    /**
     * 获取上下文
     *
     * @return
     */
    public static Context getBaseApplicationContext() {
        return baseApplicationContext;
    }


    //线程入口
    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }


    public void initService() {

        if (CWMUtils.isServiceRunning ("com.cqwo.gameplayer.framework.service.UMengPushService")) {
            //ToastUtils.showLongToast ("消息服务已经启动");
        } else {
            Intent intent = new Intent (CWMUtils.getContext (), UMengPushService.class);
            startService (intent);
        }

    }


}
