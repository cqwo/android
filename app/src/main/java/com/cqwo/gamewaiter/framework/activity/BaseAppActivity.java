package com.cqwo.gamewaiter.framework.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.cqwo.gamewaiter.R;
import com.cqwo.gamewaiter.core.constants.CWMConstans;

public abstract class BaseAppActivity extends AppCompatActivity {

    protected String TAG = "BaseAppActivity";

    protected ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);

        actionBar = getSupportActionBar ();

        //getActionBar ().setTitle (R.string.app_title);
        actionBar.setTitle (R.string.app_name);
        actionBar.setDisplayOptions (ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_HOME_AS_UP);


        Log.d (TAG, "onCreate: " + getSupportActionBar ());


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId ()) {
            case android.R.id.home:


                Log.d (TAG, "onOptionsItemSelected: 我要搞事了哟");
                this.finish ();
                // 处理返回逻辑
                return true;
            default:
                return super.onOptionsItemSelected (item);
        }
    }


}
