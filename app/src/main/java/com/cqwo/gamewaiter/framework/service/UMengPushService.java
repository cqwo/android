package com.cqwo.gamewaiter.framework.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.cqwo.gamewaiter.core.helper.StringHelper;
import com.cqwo.gamewaiter.core.message.MessageInfo;
import com.cqwo.gamewaiter.services.Messages;
import com.umeng.message.UTrack;
import com.umeng.message.UmengMessageService;
import com.umeng.message.common.UmLog;
import com.umeng.message.entity.UMessage;

import org.android.agoo.common.AgooConstants;
import org.json.JSONObject;

public class UMengPushService extends UmengMessageService {


    private static final String TAG = "UMengPushService";


    @Override
    public void onMessage(Context context, Intent intent) {


        Log.e (TAG, "onMessage: 处理消息");

        Log.d (TAG, "onMessage: " + intent.getStringExtra (AgooConstants.MESSAGE_BODY));

        //可以通过MESSAGE_BODY取得消息体
        String message = intent.getStringExtra (AgooConstants.MESSAGE_BODY);

        UMessage msg = null;

        if (StringHelper.isEmpty (message)) {
            Log.e (TAG, "onMessage: 消息为空");
            return;
        }


        try {
            msg = new UMessage (new JSONObject (message));
        } catch (Exception e) {

            //Toast.makeText (getApplicationContext (), e.getMessage (), Toast.LENGTH_LONG).show ();
            Log.e (TAG, "onMessage: ", e);
        }
        UmLog.d (TAG, "message=" + message);      //消息体
        UmLog.d (TAG, "custom=" + msg.custom);    //自定义消息的内容
        UmLog.d (TAG, "title=" + msg.title);      //通知标题
        UmLog.d (TAG, "text=" + msg.text);        //通知内容
        UmLog.d (TAG, "extra=" + msg.extra);        //通知内容

        //Toast.makeText (this, msg.text, Toast.LENGTH_LONG).show ();
//         code  to handle message here
//         ...


        // 对完全自定义消息的处理方式，点击或者忽略
        boolean isClickOrDismissed = true;
        if (isClickOrDismissed) {
            //完全自定义消息的点击统计
            UTrack.getInstance (getApplicationContext ()).trackMsgClick (msg);
        } else {
            //完全自定义消息的忽略统计
            UTrack.getInstance (getApplicationContext ()).trackMsgDismissed (msg);
        }

        Log.i (TAG, "onMessage: " + msg.extra);


        MessageInfo messageInfo = Messages.toMesssage (msg.extra.get ("message"));

        if (messageInfo == null)
            return;


        Log.d (TAG, "接收消息messageInfo: " + messageInfo.toString ());


    }
}
