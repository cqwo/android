package com.cqwo.gamewaiter.services;

/**
 * Created by cqnews on 2018/1/22.
 */

import com.alibaba.fastjson.JSON;
import com.cqwo.gamewaiter.core.message.MessageInfo;

/**
 * 消息操作类
 */
public class Messages {


    private static final String TAG = "Messages";

    /**
     * 字符转MesssageInfo
     *
     * @param s
     * @return
     */
    public static MessageInfo toMesssage(String s) {

        MessageInfo messageInfo = null;
        try {

            messageInfo = JSON.parseObject (s, MessageInfo.class);

        } catch (Exception e) {

            //Log.e(TAG, "字符转MesssageInfo: ", e);

        }

        return messageInfo;

    }


}
