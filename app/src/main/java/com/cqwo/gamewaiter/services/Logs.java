package com.cqwo.gamewaiter.services;

import android.util.Log;

import com.cqwo.gamewaiter.core.helper.LogUtils;
import com.cqwo.gamewaiter.core.helper.LogUtils;

/**
 * Created by cqnews on 2018/3/15.
 */

public class Logs {

    private static final String TAG = "Logs";


    public void debug(String s) {


        LogUtils.d ("");

        Log.d ("Tag", s);
    }

    public void error(String s) {

        LogUtils.e (s);

        Log.e (TAG, "error: " + s);
    }


}
