package com.cqwo.gamewaiter.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.cqwo.gamewaiter.core.constants.FilterConstants;
import com.cqwo.gamewaiter.core.helper.CWMUtils;
import com.cqwo.gamewaiter.core.message.MessageInfo;


/**
 * 广播
 */
public class FilterUtils {


    private static final String TAG = "FilterUtils";


    /**
     * 发送广播
     *
     * @param action
     */
    public static void send(String action) {

        send (CWMUtils.getContext (), action, new MessageInfo ());

    }

    /**
     * 发送广播
     *
     * @param action
     * @param message
     */
    public static void send(String action, String message) {
        send (CWMUtils.getContext (), action, message);
    }

    /**
     * 发送广播
     *
     * @param action
     */
    public static void send(String action, MessageInfo messageInfo) {


        send (CWMUtils.getContext (), action, messageInfo);
    }

    /**
     * 发送广播
     *
     * @param context 上下文
     * @param action  广告action
     */
    public static void send(Context context, String action) {

        send (context, action, new MessageInfo ());
    }

    /**
     * 发送广播
     *
     * @param context
     * @param action
     * @param message
     */
    public static void send(Context context, String action, String message) {

        MessageInfo messageInfo = new MessageInfo ();

        messageInfo.setMessage (message);
        messageInfo.setState (0);

        send (context, action, messageInfo);
    }

    /**
     * 发送广播
     *
     * @param context 上下文
     * @param action  广告action
     */
    public static void send(Context context, String action, MessageInfo messageInfo) {

        try {

            Intent intent = new Intent (action);

            intent.putExtra (FilterConstants.FilterParameterKey, messageInfo);


            context.sendBroadcast (intent);
            Log.e (TAG, "发送广播成功");
            Log.e (TAG, "action:" + action);

        } catch (Exception ex) {

            Log.e (TAG, "发送广播失败", ex);
        }

    }


    /**
     * 注册消息
     *
     * @param broadcastReceiver
     * @param actionArgs
     */
    public static void registerReceiver(BroadcastReceiver broadcastReceiver, String... actionArgs) {


        registerReceiver (CWMUtils.getContext (), broadcastReceiver, actionArgs);
    }

    /**
     * 注册消息
     *
     * @param broadcastReceiver
     * @param actionArgs
     */
    public static void registerReceiver(Context context, BroadcastReceiver broadcastReceiver, String... actionArgs) {


        if (actionArgs.length <= 0)
            return;

        IntentFilter intentFilter = new IntentFilter ();

        for (String s : actionArgs) {

            intentFilter.addAction (s);
        }

        registerReceiver (context, intentFilter, broadcastReceiver);

    }

    /**
     * 注册消息
     *
     * @param intentFilter
     * @param broadcastReceiver
     */
    public static void registerReceiver(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {


        registerReceiver (CWMUtils.getContext (), intentFilter, broadcastReceiver);

    }

    /**
     * 注册消息
     *
     * @param context
     * @param intentFilter
     * @param broadcastReceiver
     */
    public static void registerReceiver(Context context, IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {

        try {

            context.registerReceiver (broadcastReceiver, intentFilter);
        } catch (Exception ex) {
            Log.e (TAG, "注册广播失败: ", ex);
        }
    }

    /**
     * 注消注册消息
     *
     * @param broadcastReceiver
     */
    public static void unRegisterReceiver(BroadcastReceiver broadcastReceiver) {
        unRegisterReceiver (CWMUtils.getContext (), broadcastReceiver);
    }

    /**
     * 注消注册消息
     *
     * @param context
     * @param broadcastReceiver
     */
    public static void unRegisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {

        try {

            context.unregisterReceiver (broadcastReceiver);
        } catch (Exception ex) {

            Log.e (TAG, "注消广播失败: ", ex);
        }
    }


}
