package com.cqwo.gamewaiter.services;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.cqwo.gamewaiter.R;
import com.cqwo.gamewaiter.core.domian.SelectListItem;

import java.util.List;

public abstract class PickerUtils {


    private OptionsPickerView pickerView;

    public abstract List<SelectListItem> getOptionList();


    protected abstract void onSelect(SelectListItem item, int postion);

    public PickerUtils(Context context) {


        /**
         * @description
         *
         * 注意事项：
         * 自定义布局中，id为 optionspicker 或者 timepicker 的布局以及其子控件必须要有，否则会报空指针。
         * 具体可参考demo 里面的两个自定义layout布局。
         */
        pickerView = new OptionsPickerBuilder (context, new OnOptionsSelectListener () {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3, View v) {

                SelectListItem item = getOptionList ().get (options1);

                if (item == null)
                    return;

                //返回的分别是三个级别的选中位置
                String tx = item.getPickerViewText ();


                onSelect (item, options1);

            }
        })
                .setLayoutRes (R.layout.pickerview_custom_options, new CustomListener () {
                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById (R.id.tv_finish);
                        ImageView ivCancel = (ImageView) v.findViewById (R.id.iv_cancel);
                        tvSubmit.setOnClickListener (new View.OnClickListener () {
                            @Override
                            public void onClick(View v) {
                                pickerView.returnData ();
                                pickerView.dismiss ();
                            }
                        });

                        ivCancel.setOnClickListener (new View.OnClickListener () {
                            @Override
                            public void onClick(View v) {
                                pickerView.dismiss ();
                            }
                        });

                    }
                })
                .isDialog (false)
                .build ();

        pickerView.setPicker (getOptionList ());//添加数据


        for (int i = 0; i < getOptionList ().size (); i++) {

            if (getOptionList ().get (i).selected) {

                pickerView.setSelectOptions (i);
                break;
            }
        }

    }

    public void show() {
        pickerView.show ();
    }


}


