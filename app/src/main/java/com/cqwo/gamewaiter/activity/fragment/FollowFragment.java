package com.cqwo.gamewaiter.activity.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cqwo.gamewaiter.R;

public class FollowFragment extends Fragment {

    private static final String TAG = "FollowFragment";

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.e (TAG, "我的关注加载");

        view = inflater.inflate (R.layout.fragment_follow, container, false);

        return view;

    }
}
