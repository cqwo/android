package com.cqwo.gamewaiter.activity.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cqwo.gamewaiter.R;
import com.cqwo.gamewaiter.core.domian.BannerInfo;
import com.zhouwei.mzbanner.MZBannerView;
import com.zhouwei.mzbanner.holder.MZHolderCreator;
import com.zhouwei.mzbanner.holder.MZViewHolder;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";

    private Context mContext;

    private View homeView;


    private MZBannerView bannerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.e (TAG, "首页加载");


        homeView = inflater.inflate (R.layout.fragment_home, container, false);

        bannerView = homeView.findViewById (R.id.homeBanner);

        bannerView.setPages (mockBannerData (), new MZHolderCreator<BannerViewHolder> () {
            @Override
            public BannerViewHolder createViewHolder() {
                return new BannerViewHolder ();
            }
        });

        return homeView;

    }

    public static class BannerViewHolder implements MZViewHolder<BannerInfo> {

        private ImageView mImageView;

        @Override
        public View createView(Context context) {
            // 返回页面布局
            View view = LayoutInflater.from (context).inflate (R.layout.banner_item, null);
            mImageView = (ImageView) view.findViewById (R.id.banner_image);
            return view;
        }

        @Override
        public void onBind(Context context, int position, BannerInfo data) {
            // 数据绑定
            mImageView.setImageResource (data.image);


        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach (activity);
        this.mContext = activity;
    }

    @Override
    public void onPause() {
        super.onPause ();
        bannerView.pause ();//暂停轮播
    }

    @Override
    public void onResume() {
        super.onResume ();
        bannerView.start ();//开始轮播
    }


    public List<BannerInfo> mockBannerData() {
        List<BannerInfo> datas = new ArrayList<> ();
        for (int i = 1; i <= 4; i++) {
            BannerInfo info = new BannerInfo ();
            info.title = "公众号: Android技术杂货铺";
            info.image = getResources ().getIdentifier ("banner" + i, "mipmap", mContext.getPackageName ());
            ;
            datas.add (info);
        }
        return datas;
    }


}
