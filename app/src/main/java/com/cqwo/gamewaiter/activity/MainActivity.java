package com.cqwo.gamewaiter.activity;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.cqwo.gamewaiter.R;
import com.cqwo.gamewaiter.activity.fragment.FollowFragment;
import com.cqwo.gamewaiter.activity.fragment.HomeFragment;
import com.cqwo.gamewaiter.activity.fragment.MessageFragment;
import com.cqwo.gamewaiter.activity.fragment.MineFragment;
import com.cqwo.gamewaiter.framework.activity.BaseAppActivity;
import com.cqwo.gamewaiter.services.FilterUtils;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends BaseAppActivity {

    MainBroadcastReceiver broadcastReceiver;

    BottomBar mBottomBar;

    /**
     * 首页模型
     */
    HomeFragment homeFragment;

    /**
     * 关注模型
     */
    FollowFragment followFragment;

    /**
     * 消息模型
     */
    MessageFragment messageFragment;

    /**
     * 个人中心模型
     */
    MineFragment mineFragment;

    /**
     * 框架管理器
     */
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);

        initUI ();
        initFilter ();

    }

    private void initFilter() {

        broadcastReceiver = new MainBroadcastReceiver ();

        FilterUtils.registerReceiver (
                MainActivity.this,
                broadcastReceiver);
    }

    @SuppressLint("ResourceAsColor")
    private void initUI() {

        setContentView (R.layout.activity_main);
        actionBar.hide ();
        setDefaultFragment ();
        mBottomBar = (BottomBar) findViewById (R.id.mainBottomBar);

        mBottomBar.getTabAtPosition (3).setBadgeCount (3);
        mBottomBar.getTabAtPosition (1).setBadgeCount (1);
        setDefaultFragment ();

        mBottomBar.setOnTabSelectListener (new OnTabSelectListener () {

            @Override
            public void onTabSelected(int tabId) {


                FragmentTransaction transaction = fragmentManager.beginTransaction ();
                transaction.setTransition (FragmentTransaction.TRANSIT_NONE); // fragment切换动画
                hideFragments (transaction);

                if (tabId == R.id.tab_home) {


                    if (homeFragment == null) {
                        homeFragment = new HomeFragment ();
                        transaction.add (R.id.mainContainer, homeFragment);
                    } else {
                        transaction.show (homeFragment);
                    }
                    Log.e (TAG, "onTabSelected:我点的是首页 ");

                } else if (tabId == R.id.tab_follow) {

                    Log.e (TAG, "onTabSelected:我的关注 ");

                    if (followFragment == null) {
                        followFragment = new FollowFragment ();
                        transaction.add (R.id.mainContainer, followFragment);
                    } else {
                        transaction.show (followFragment);
                    }

                } else if (tabId == R.id.tab_send) {

                    Log.e (TAG, "onTabSelected:发起处理消息 ");
                } else if (tabId == R.id.tab_message) {

                    Log.e (TAG, "onTabSelected:我的消息 ");


                    if (messageFragment == null) {
                        messageFragment = new MessageFragment ();
                        transaction.add (R.id.mainContainer, messageFragment);
                    } else {
                        transaction.show (messageFragment);
                    }

                } else if (tabId == R.id.tab_mine) {

                    Log.e (TAG, "onTabSelected:我的个人中心 ");

                    if (mineFragment == null) {
                        mineFragment = new MineFragment ();
                        transaction.add (R.id.mainContainer, mineFragment);
                    } else {
                        transaction.show (mineFragment);
                    }

                } else {

                    Log.e (TAG, "onTabSelected:我不知道我能干什么? ");
                }


                transaction.commit();

            }
        });

    }

    /**
     * 设置默认Fragment
     */
    private void setDefaultFragment() {

        fragmentManager = getFragmentManager ();

        FragmentTransaction transaction = fragmentManager.beginTransaction ();
        homeFragment = new HomeFragment ();
        transaction.add (R.id.mainContainer, homeFragment);
        transaction.commit ();
    }

    private void hideFragments(FragmentTransaction transaction) {

        if (homeFragment != null) {

            transaction.hide (homeFragment);
            Log.i (TAG, "隐藏了home");
        }
        if (followFragment != null) {

            transaction.hide (followFragment);
            Log.i (TAG, "隐藏了labers");
        }
        if (messageFragment != null) {

            transaction.hide (messageFragment);
            Log.i (TAG, "隐藏了bill");
        }
        if (mineFragment != null) {

            transaction.hide (mineFragment);
            Log.i (TAG, "隐藏了center");
        }

    }

    /**
     * 首页广播
     */
    class MainBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction ();


        }
    }
}
