package com.cqwo.gamewaiter.activity.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cqwo.gamewaiter.R;

public class MineFragment extends Fragment {

    private static final String TAG = "MineFragment";

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.e (TAG, "我的关注加载");

        view = inflater.inflate (R.layout.fragment_mine, container, false);

        return view;

    }
}
