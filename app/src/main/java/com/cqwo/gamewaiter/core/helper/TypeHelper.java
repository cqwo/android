package com.cqwo.gamewaiter.core.helper;

import android.util.Log;

/**
 * Created by Admin on 2017/12/13.
 */

public class TypeHelper {

    private static final String TAG = "TypeHelper";

    //把long类型的毫秒转换为int类型的秒
    public static int TimeMsToSecond(long Ms) {

        int ms = new Long (Ms).intValue ();
        int S = ms / 1000;

        return S;
    }

    public static int LongToInt(long value) {

        int timeValue = new Long (value).intValue ();

        return timeValue;
    }

    public static String IntToString(int i) {

        String s = i + "";
        return s.trim ();

    }

    public static int StringToInt(Object s) {
        return StringToInt (s, 0);
    }


    /// <summary>
    /// 将string类型转换成int类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static int StringToInt(Object s, int defaultValue) {
        try {

            if (s != null && String.valueOf (s).trim ().length () > 0)
                return Integer.parseInt (s.toString ());
            else
                return defaultValue;

        } catch (Exception e) {
            Log.e ("ERROR", e.toString ());
            return defaultValue;
        }
    }

    public static int ObjectToInt(Object o) {

        return ObjectToInt (o, 0);
    }

    public static int ObjectToInt(Object o, int defaultValue) {
        try {
            return (int) o;
        } catch (Exception e) {
            return defaultValue;
        }
    }


    /// <summary>
    /// 将string类型转换成int类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static String IntToString(Object s, String defaultValue) {
        try {

            if (s != null)
                return String.valueOf (s.toString ());

            else
                return defaultValue;

        } catch (Exception e) {
            Log.e ("ERROR", e.toString ());
            return defaultValue;
        }
    }

    /**
     * char 转字符串
     *
     * @param c
     * @return
     */
    public static String CharToString(char c) {
        try {
            return String.valueOf (c);
        } catch (Exception e) {
            return "";
        }
    }


    /// <summary>
    /// 将int类型转换成string类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static long StringToLong(String s) {
        return StringToLong (s, 0);
    }

    /// <summary>
    /// 将int类型转换成string类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static long StringToLong(String s, long defaultValue) {
        try {

            if (s != null)
                return Long.parseLong (s.toString ());
            else
                return defaultValue;

        } catch (Exception e) {
            Log.e ("ERROR", e.toString ());
            return defaultValue;
        }
    }

    /// <summary>
    /// 将int类型转换成string类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static String LongToString(long s) {
        return LongToString (s, "0");
    }

    /// <summary>
    /// 将string类型转换成int类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static String LongToString(Object s, String defaultValue) {
        try {

            if (s != null)
                return String.valueOf (s.toString ());

            else
                return defaultValue;

        } catch (Exception e) {
            Log.e ("ERROR", e.toString ());
            return defaultValue;
        }
    }

    /// <summary>
    /// 将string类型转换成double类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static double StringToDouble(String s) {
        return StringToDouble (s, 0);
    }

    /// <summary>
    /// 将string类型转换成double类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static double StringToDouble(Object s, int defaultValue) {
        try {

            if (s != null)
                return Double.parseDouble (s.toString ());
            else
                return defaultValue;

        } catch (Exception e) {
            Log.e ("ERROR", e.toString ());
            return defaultValue;
        }
    }


    /// <summary>
    /// 将string类型转换成double类型
    /// </summary>
    /// <param name="s">目标字符串</param>
    /// <returns></returns>
    public static String DoubleToString(double s) {
        return DoubleToString (s, "");
    }

    public static String DoubleToString(Object s, String defaultValue) {

        try {

            if (s != null)
                return String.valueOf (s.toString ());

            else
                return defaultValue;

        } catch (Exception e) {
            Log.e ("ERROR", e.toString ());
            return defaultValue;
        }

    }


    public static int LongToInt(Long value) {
        return LongToInt (value, 0);
    }

    public static int LongToInt(Long value, int defaultValue) {
        try {
            return new Long (value).intValue ();
        } catch (Exception e) {

            return defaultValue;
        }
    }


    public static int DoubleToInt(double value) {
        return DoubleToInt (value, 0);
    }

    private static int DoubleToInt(double value, int defaultValue) {
        try {
            return new Double (value).intValue ();
        } catch (Exception e) {

            return defaultValue;
        }
    }


}
