package com.cqwo.gamewaiter.core.domian;

public class ActionInfo {

    /**
     * 类型
     */
    private int type = 0;

    /**
     * 动作
     */
    private String action = "";

    /**
     * 标题
     */
    private int title = 0;

    /**
     * 副标题
     */
    private String subTitle = "";

    /**
     * 图标
     */
    private int icon = 0;


    public ActionInfo() {
    }


    public ActionInfo(String action, int title, int icon) {
        this.action = action;
        this.title = title;
        this.icon = icon;
    }

    public ActionInfo(int type, String action, int title, String subTitle) {
        this.type = type;
        this.action = action;
        this.title = title;
        this.subTitle = subTitle;
    }

    public ActionInfo(int type, String action, int title, String subTitle, int icon) {
        this.type = type;
        this.action = action;
        this.title = title;
        this.subTitle = subTitle;
        this.icon = icon;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "ActionInfo{" +
                "action='" + action + '\'' +
                ", title='" + title + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }

}
