package com.cqwo.gamewaiter.core.helper;

import android.nfc.Tag;
import android.util.Log;

/**
 * Created by cqnews on 2018/1/22.
 */

public class Logger {


    private String tag;

    public Logger(String tag) {
        this.tag = tag;
    }

    public static Logger t(String tag) {

        return new Logger(tag);
    }


    /**
     * d输出
     *
     * @param s
     */
    public void d(String s) {
        Log.d(tag, s);
    }
}
