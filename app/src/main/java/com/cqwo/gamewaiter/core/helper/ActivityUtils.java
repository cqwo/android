package com.cqwo.gamewaiter.core.helper;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.text.TextUtils;

import java.util.List;

/**
 * <pre>
 *     author: Blankj
 *     blog  : http://blankj.com
 *     time  : 2016/9/23
 *     desc  : Activity相关工具类
 * </pre>
 */
public class ActivityUtils {

    private ActivityUtils() {
        throw new UnsupportedOperationException ("u can't instantiate me...");
    }

    /**
     * 判断是否存在Activity
     *
     * @param context     上下文
     * @param packageName 包名
     * @param className   activity全路径类名
     * @return {@code true}: 是<br>{@code false}: 否
     */
    public static boolean isActivityExists(Context context, String packageName, String className) {
        Intent intent = new Intent ();
        intent.setClassName (packageName, className);
        return !(context.getPackageManager ().resolveActivity (intent, 0) == null ||
                intent.resolveActivity (context.getPackageManager ()) == null ||
                context.getPackageManager ().queryIntentActivities (intent, 0).size () == 0);
    }

    /**
     * 打开Activity
     *
     * @param context     上下文
     * @param packageName 包名
     * @param className   全类名
     */
    public static void launchActivity(Context context, String packageName, String className) {
        launchActivity (context, packageName, className, null);
    }

    /**
     * 打开Activity
     *
     * @param context     上下文
     * @param packageName 包名
     * @param className   全类名
     * @param bundle      bundle
     */
    public static void launchActivity(Context context, String packageName, String className, Bundle bundle) {
        context.startActivity (IntentUtils.getComponentIntent (packageName, className, bundle));
    }

    /**
     * 获取launcher activity
     *
     * @param context     上下文
     * @param packageName 包名
     * @return launcher activity
     */
    public static String getLauncherActivity(Context context, String packageName) {
        Intent intent = new Intent (Intent.ACTION_MAIN, null);
        intent.addCategory (Intent.CATEGORY_LAUNCHER);
        PackageManager pm = context.getPackageManager ();
        List<ResolveInfo> infos = pm.queryIntentActivities (intent, 0);
        for (ResolveInfo info : infos) {
            if (info.activityInfo.packageName.equals (packageName)) {
                return info.activityInfo.name;
            }
        }
        return "no " + packageName;
    }

    /**
     * 判断某个界面是否在前台
     *
     * @param className 要判断的Activity
     * @return 是否在前台显示
     */
    public static boolean isForeground(String className) {
        return isForeground (CWMUtils.getContext (), className);
    }

    /**
     * 判断某个界面是否在前台
     *
     * @param context   Context
     * @param className 界面的类名
     * @return 是否在前台显示
     */
    public static boolean isForeground(Context context, String className) {
        if (context == null || TextUtils.isEmpty (className))
            return false;
        ActivityManager am = (ActivityManager) context.getSystemService (Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks (1);
        if (list != null && list.size () > 0) {
            ComponentName cpn = list.get (0).topActivity;
            if (className.equals (cpn.getClassName ()))
                return true;
        }
        return false;
    }
}
