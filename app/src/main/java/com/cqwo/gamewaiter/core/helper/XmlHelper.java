package com.cqwo.gamewaiter.core.helper;

import android.util.Log;

import com.thoughtworks.xstream.XStream;


/**
 * Created by cqnews on 2018/1/21.
 */

public class XmlHelper {

    final static String TAG = "XmlHelper";

    final static XStream xStrem = new XStream();

    

    /**
     * 实体转XML
     *
     * @param info
     * @return
     */
    public static String toXML(Object info) {


        String s = "";
        try {
            s = xStrem.toXML(info);
        } catch (Exception e) {
            Log.e(TAG, "toXML: ", e);
        }

        return s;
    }

    /**
     * 保存实体XML到本地
     *
     * @param info
     * @param fileName
     * @return
     */
    public static boolean saveXml(Object info, String fileName) {

        try{

            String s = toXML(info);

            FileHelper.saveFile(fileName, s);
        }catch (Exception e){

            Log.e(TAG, "保存实体XML到本地: ", e);
            return false;
        }

        return true;
    }


    /**
     * xml转实体类
     *
     * @param s
     * @return
     */
    public static Object fromXML(String s) {
        try {
            return (Object) xStrem.fromXML(s);
        } catch (Exception e) {
            Log.e(TAG, "xml转实体类: ", e);
        }
        return null;
    }

    /**
     * 从保存的数据中读取模型
     *
     * @param fileName
     * @return
     */
    public static Object readXml(String fileName) {

        try {
            String s = FileHelper.readFile(fileName);

            return fromXML(s);

        } catch (Exception e) {
            Log.e(TAG, "xml转实体类: ", e);
        }
        return null;


    }

}
