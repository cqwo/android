package com.cqwo.gamewaiter.core.helper;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import com.cqwo.gamewaiter.core.constants.CWMConstans;

import java.text.MessageFormat;
import java.util.ArrayList;

/**
 * <pre>
 *     author: Blankj
 *     blog  : http://blankj.com
 *     time  : 16/12/08
 *     desc  : Utils初始化相关
 * </pre>
 */
public class CWMUtils {

    private static Context context;
    private static String TAG = "CWMUtils";

    private CWMUtils() {
        throw new UnsupportedOperationException ("u can't instantiate me...");
    }

    /**
     * 初始化工具类
     *
     * @param context 上下文
     */
    public static void init(Context context) {
        CWMUtils.context = context.getApplicationContext ();
        ToastUtils.init (true);
    }

    /**
     * 获取ApplicationContext
     *
     * @return ApplicationContext
     */
    public static Context getContext() {
        if (context != null) return context;
        throw new NullPointerException ("u should init first");
    }



    /**
     * 处理url
     *
     * @param url
     * @return
     */
    public static String disposeUrl(String url) {

        if (url.contains ("https://") || url.contains ("http://")) {

            url = url.replaceAll ("http:/", "http://");
            url = url.replaceAll ("https:/", "https://");

            url = url.replaceAll ("//", "/");


            url = url.replaceAll ("http://https://", "https://");
            url = url.replaceAll ("https://http://", "https://");
            url = url.replaceAll ("http://http://", "https://");

        } else {
            url = MessageFormat.format ("{0}{1}", CWMConstans.apiUrl, url);
        }
        return url;
    }

    /**
     * 获取字符型资源数据
     */
    public static String getString(int resId) {

        try {
            return getContext ().getString (resId);

        } catch (Exception ex) {

            Log.d (TAG, "获取字符型资源数据: " + ex);
        }

        return "";
    }

    public static boolean isServiceRunning(String ServiceName) {
        try {

            if (("").equals (ServiceName) || ServiceName == null)
                return false;

            ActivityManager myManager = (ActivityManager) getContext ()
                    .getSystemService (Context.ACTIVITY_SERVICE);
            ArrayList<ActivityManager.RunningServiceInfo> runningService = (ArrayList<ActivityManager.RunningServiceInfo>) myManager
                    .getRunningServices (30);
            for (int i = 0; i < runningService.size (); i++) {
                if (runningService.get (i).service.getClassName ().toString ()
                        .equals (ServiceName)) {
                    return true;
                }
            }
        } catch (Exception e) {
            Log.e (TAG, "判断服务是否开启: ", e);
        }

        return false;
    }
}