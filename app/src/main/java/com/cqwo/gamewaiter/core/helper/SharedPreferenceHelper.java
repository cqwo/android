package com.cqwo.gamewaiter.core.helper;

import android.content.SharedPreferences;
import android.util.Log;

import com.cqwo.gamewaiter.framework.activity.BaseApplication;

/**
 * Created by cqnews on 2018/1/21.
 */

public class SharedPreferenceHelper {


    private static final String TAG = "SharedPreferenceHelper";


    /**
     * Int写入SharedPreference
     *
     * @param name
     * @param value
     */
    public static void writeSharedPreference(String name, int value) {
        String s = TypeHelper.IntToString (value);
        writeSharedPreference (name, s);
    }

    public static int readSharedPreference(String name, int defaultValue) {

        String s = TypeHelper.IntToString (defaultValue);

        s = readSharedPreference (name, s);


        return TypeHelper.StringToInt (s, defaultValue);

    }

    /**
     * 写入SharedPreference
     *
     * @param name  名称
     * @param value 值
     */
    public static void writeSharedPreference(String name, String value) {

        try {
            SharedPreferences.Editor editor = BaseApplication.sp.edit ();
            editor.putString (name, value);
            editor.commit ();

            SPUtils spUtils = new SPUtils ("1");


        } catch (Exception e) {

            Log.e (TAG, "写入SharedPreference失败： ", e);
        }

    }

    /**
     * 读取SharedPreference
     *
     * @param name
     * @param defaultValue
     * @return
     */
    public static String readSharedPreference(String name, String defaultValue) {

        String s = "";
        try {
            s = BaseApplication.sp.getString (name, defaultValue);

        } catch (Exception e) {

            Log.e (TAG, "写入SharedPreference失败： ", e);
        }

        return s;
    }

}
