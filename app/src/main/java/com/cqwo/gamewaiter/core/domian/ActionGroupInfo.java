package com.cqwo.gamewaiter.core.domian;

import java.util.List;

public class ActionGroupInfo {


    /**
     * 分组名称
     */
    private ActionInfo groupName;

    /**
     * 动作列表
     */
    List<ActionInfo> actionInfoList;


    public ActionGroupInfo(ActionInfo groupName) {
        this.groupName = groupName;
    }

    public ActionInfo getGroupName() {
        return groupName;
    }

    public void setGroupName(ActionInfo groupName) {
        this.groupName = groupName;
    }

    public List<ActionInfo> getActionInfoList() {
        return actionInfoList;
    }

    public void setActionInfoList(List<ActionInfo> actionInfoList) {
        this.actionInfoList = actionInfoList;
    }

    /**
     * 获取Item内容
     *
     * @param pPosition
     * @return
     */
    public ActionInfo getItem(int pPosition) {
        // Category排在第一位
        if (pPosition == 0) {
            return groupName;
        } else {
            return actionInfoList.get (pPosition - 1);
        }
    }

    /**
     * 当前类别Item总数。Category也需要占用一个Item
     *
     * @return
     */
    public int getItemCount() {
        return actionInfoList.size () + 1;
    }
}
