package com.cqwo.gamewaiter.core.helper;

import android.util.Log;

/**
 * 进制转换
 */
public class HexUtils {


    private static final String TAG = "HexUtils";

    /**
     * 十六进制转二进制
     *
     * @param s 原串
     * @return
     */
    public static String hexadecimalToBinary(String s) {

        int length = s.length () * 4;
        return hexadecimalToBinary (s, length);

    }

    /**
     * 十六进制转二进制
     *
     * @param s      原串
     * @param length 长度
     * @return
     */
    public static String hexadecimalToBinary(String s, int length) {

        try {

            int i = hexadecimalToDecimalism (s);

            return toBinary (i, length);

        } catch (Exception ex) {

        }
        return "";
    }

    /**
     * 二进度转十六进制
     *
     * @return
     */
    public static String binaryToHexadecimal(String s) {

        try {

            /**
             * 行转十进制
             */
            int i = binaryToDecimalism (s);

            return Integer.toHexString (i);

        } catch (Exception ex) {

        }
        return "";


    }

    //region 其它转十进度

    //region 二进度转十进制


    /**
     * 十进制转二进制
     *
     * @param s      字符串
     * @param length 长度
     * @return
     */
    public static String toBinary(int s, int length) {

        try {
            //十进制 -> 二进制
            String str = Integer.toBinaryString (s);

            while (str.length () < length) {
                str = 0 + str;
            }

            return str;

        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * 二进度转十进制
     *
     * @param s
     * @return
     */
    public static int binaryToDecimalism(String s) {

        return binaryToDecimalism (s, 0);
    }

    /**
     * 二进度转十进制
     *
     * @param s
     * @param defaultValue
     * @return
     */
    public static int binaryToDecimalism(String s, int defaultValue) {

        try {
            s = binaryFillZore (s);

            return Integer.parseInt (s, 2);

        } catch (Exception ex) {

        }

        return defaultValue;
    }


    /**
     * 二进度填充位数
     *
     * @param s
     * @return
     */
    public static String binaryFillZore(String s) {

        int n = 4 - s.length () % 4;

        System.out.println (n);

        if (n >= 1) {

            for (int i = 0; i < n; i++) {
                s = "0" + s;
            }

        }

        return s;
    }

    //endregion

    //region 十六进制转十进

    /**
     * 十六进制转十进度
     *
     * @return
     */
    public static int hexadecimalToDecimalism(String s) {
        return hexadecimalToDecimalism (s, 0);
    }

    /**
     * 十六进制转十进度
     *
     * @return
     */
    public static int hexadecimalToDecimalism(String s, int defaultValue) {

        try {
            return Integer.parseInt (s, 16);

        } catch (Exception ex) {

        }

        return defaultValue;
    }

    //endregion

    /**
     * 获取校验和cs
     *
     * @param s
     * @return
     */
    public static String hexAddtion(String s) {

        String hexCS = "";

        try {
            int tenCS = 0;

            if (s.length () % 2 != 0) {
                s = "0" + s;
            }

            for (int i = 0; i < s.length (); i++) {
                if (i % 2 == 0) {
                    String everyHex = StringHelper.subString (s, i, 2);
                    int everyTen = hexadecimalToDecimalism (everyHex);
                    tenCS += everyTen;
                }
            }

            String binaryCS = toBinary (tenCS, 3);

            hexCS = binaryToHexadecimal (binaryCS);

            if (hexCS.length () > 2) {
                hexCS = StringHelper.subString (hexCS, hexCS.length () - 2, 2);
            }

            if (hexCS.length () < 2) {
                hexCS = "0" + hexCS;
            }

        } catch (Exception e) {

        }

        s += hexCS;

        return s.toUpperCase ();
    }

    //endregion

    /**
     * 二进进位移运算
     *
     * @param i1
     * @param i2
     */
    public static int shiftoperation(int i1, int i2) {

        try {

            return i1 & i2;

        } catch (Exception ex) {

            Log.e (TAG, "位移动算: ", ex);
        }

        return -1;

    }

    /**
     * 二进进位移运算
     *
     * @param s1
     * @param s2
     */
    public static int binaryShiftoperation(String s1, String s2) {

        int i1 = binaryToDecimalism (s1);
        int i2 = binaryToDecimalism (s2);

        return shiftoperation (i1, i2);

    }


    /**
     * 十六进进位移运算
     *
     * @param s1
     * @param s2
     */
    public static int hexadecimalShiftoperation(String s1, String s2) {

        int i1 = hexadecimalToDecimalism (s1);
        int i2 = hexadecimalToDecimalism (s2);

        return shiftoperation (i1, i2);

    }

    /**
     * 十六进制转ASC码
     *
     * @param s
     * @return
     */
    public static String hexadecimalShiftToASCII(String s) {

        try {

            int i = hexadecimalToDecimalism (s);

        } catch (Exception ex) {

        }

        return "";

    }




    public static String convertHexToString(String hex){

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for( int i=0; i<hex.length()-1; i+=2 ){

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char)decimal);

            temp.append(decimal);
        }

        return sb.toString();
    }



    //region 位移

    //endregion

    /**
     * @param args
     * 把char字符型数字转成int数字，因为他们的ascii码值恰好相差48，
     * 因此把char型数字减去48得到int型数据，例如'4'转换成了4
     * '0'的ascii码是48
     */
    public static void main(String[] args) {
        System.out.println("ASCII : " + HexUtils.convertHexToString("4D4A323636"));
    }

    public static void main2(String[] args) {

        System.out.println (hexadecimalToBinary ("60"));


        String s1 = "60";

        String s2 = "80";

        String s3 = "40";

        int i1 = hexadecimalToDecimalism (s1);
        int i2 = hexadecimalToDecimalism (s2);
        int i3 = hexadecimalToDecimalism (s3);

        System.out.println (i1 & i2);

        int i4 = i1 & i3;


        System.out.println (toBinary (i4, 8));


        System.out.println (9 & 8);

    }


}
