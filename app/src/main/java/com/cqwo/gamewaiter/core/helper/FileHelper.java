package com.cqwo.gamewaiter.core.helper;

import android.os.Environment;
import android.util.Log;

import org.apache.http.util.EncodingUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


/**
 * Created by cqnews on 2018/1/21.
 */

public class FileHelper {

    static final String TAG = "FileHelper";

    /**
     * 写入文件流
     *
     * @param fileName 文件名
     * @param s        文件内容
     * @return
     */
    public static boolean saveFile(String fileName, String s) {
        try {
            File file = new File(CWMUtils.getContext ().getFilesDir(), fileName);

            Log.d(TAG, "getDataDirectory: " + Environment.getDataDirectory());

            FileOutputStream fos = new FileOutputStream(file);

            fos.write(s.getBytes());

            fos.close();

            Log.d(TAG, "saveFile: 保存文件成功");
        } catch (Exception e) {
            Log.e(TAG, "saveFile: 保存文件失败", e);
        }
        return true;
    }

    /**
     * 文件类容读取
     *
     * @param fileName
     * @return
     */
    public static String readFile(String fileName) {

        String s = "";

        try {

            File file = new File(CWMUtils.getContext ().getFilesDir(), fileName);

            FileInputStream in = new FileInputStream(file);

            int length = in.available();

            byte[] buffer = new byte[length];
            in.read(buffer);

            s = EncodingUtils.getString(buffer, "UTF-8");

            in.close();

        } catch (Exception e) {
            Log.e(TAG, "文件类容读取: ", e);
        }

        return s;

    }


}
