package com.cqwo.gamewaiter.core.helper;

import android.os.Handler;
import android.util.Log;
import android.widget.Toast;


/**
 * Created by cqnews on 2018/1/22.
 */

public class ToastHelper {

    private static final String TAG = "ToastHelper";

    // 声明一个Handler对象
    private static Handler handler = new Handler ();

    public static void show(final String message) {
        Log.d (TAG, "test: 你回来么?");
        // 新启动一个子线程
        Toast.makeText (CWMUtils.getContext (), message, Toast.LENGTH_LONG).show ();
    }
}
