package com.cqwo.gamewaiter.core.helper;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cqwo.gamewaiter.framework.activity.BaseApplication;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cqnews on 2018/1/23.
 */

public class VolleyHelper extends StringRequest {


    private static final String TAG = "VolleyHelper";

    public VolleyHelper(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super (method, url, listener, errorListener);
    }

    /**
     * 默认监听
     */
    private static Response.Listener<String> defaultListen = new Response.Listener<String> () {
        @Override
        public void onResponse(String s) {
            Log.i (TAG, "默认监听 ");
        }
    };

    /**
     * 默认错误监听
     */
    private static Response.ErrorListener defaultErrorListener = new Response.ErrorListener () {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            Log.e (TAG, "默认错误监听: ", volleyError);
        }
    };

    /**
     * 网络请求
     *
     * @param url url地址
     */
    public static void request(String url) {

        request (url, Method.POST, defaultListen, defaultErrorListener);
    }

    /**
     * 网络请求
     *
     * @param url   url地址
     * @param parms 参数列表
     */
    public static void request(String url, final Map<String, String> parms) {
        request (url, parms, defaultListen, defaultErrorListener);
    }

    /**
     * 网络请求
     *
     * @param url      url地址
     * @param listener 参数列表
     */
    public static void request(String url, Response.Listener<String> listener) {
        request (url, null, listener, defaultErrorListener);
    }

    /**
     * 网络请求
     *
     * @param url      url地址
     * @param parms    参数列表
     * @param listener 正确的监听
     */
    public static void request(String url, final Map<String, String> parms, Response.Listener<String> listener) {

        request (url, parms, listener, defaultErrorListener);
    }

    /**
     * 网络请求
     *
     * @param url           url地址
     * @param listener      正确的监听
     * @param errorListener 错误的监听
     */
    public static void request(String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        request (url, Method.POST, null, listener, errorListener);
    }

    /**
     * 网络请求
     *
     * @param url           url地址
     * @param method        请求的Method
     * @param listener      正确的监听
     * @param errorListener 错误的监听
     */
    public static void request(String url, int method, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        request (url, method, null, listener, errorListener);
    }


    /**
     * 网络请求
     *
     * @param url           url地址
     * @param parms         参数列表
     * @param listener      正确的监听
     * @param errorListener 错误的监听
     */
    public static void request(String url, final Map<String, String> parms, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        request (url, Method.POST, parms, listener, errorListener);
    }


    /**
     * 网络请求
     *
     * @param url           url地址
     * @param method        请求的Method
     * @param parms         参数列表
     * @param listener      正确的监听
     * @param errorListener 错误的监听
     */
    public static void request(String url, int method, final Map<String, String> parms, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        try {

            if (StringHelper.isEmpty (url)) {
                errorListener = new Response.ErrorListener () {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        volleyError = new VolleyError ("URL不能为空");
                    }
                };
                return;
            }

            url = CWMUtils.disposeUrl (url);

            Log.i (TAG, "request: " + url);

            Request sr = new StringRequest (method, url, listener, errorListener) {

                @Override
                protected Map<String, String> getParams() {




                    Log.i (TAG, "getParams: " + parms);

                    return parms;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<String, String> ();
                    params.put ("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }
            };

            BaseApplication.getRequestQueue ().add (sr);

        } catch (Exception ex) {

            Log.i (TAG, "网络请求错误.");
        }
    }

}
