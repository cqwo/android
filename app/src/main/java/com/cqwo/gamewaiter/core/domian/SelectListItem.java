package com.cqwo.gamewaiter.core.domian;

import com.contrarywind.interfaces.IPickerViewData;

public class SelectListItem implements IPickerViewData {

    /**
     * 值
     */
    public String value = "";

    /**
     * 文本
     */
    public String text = "";

    /**
     * 如果选定此项，则为 true；否则为 false。
     */
    public boolean selected = false;


    public SelectListItem(String value, String text, boolean selected) {
        this.value = value;
        this.text = text;
        this.selected = selected;
    }

    public SelectListItem(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public SelectListItem() {

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String getPickerViewText() {
        return text;
    }


}
