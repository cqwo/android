package com.cqwo.gamewaiter.core.message;

import android.content.Intent;

import com.cqwo.gamewaiter.core.constants.FilterConstants;

import java.io.Serializable;

public class MessageInfo implements Serializable {
    /**
     * 消息分类
     */
    private int type = 0;


    /**
     * 消息状态 -1为常态，0为成功，正常值为错误码
     */
    private int state = -1;

    /**
     * 消息说明
     */
    private String message = "力帆集团欢迎您";


    /**
     * 消息正文
     */
    private Object content;

    public MessageInfo() {
    }

    public MessageInfo(String message) {
        this.message = message;
    }


    public MessageInfo(int type, int state, String message) {
        this.type = type;
        this.state = state;
        this.message = message;
    }

    public MessageInfo(int type, int state, String message, Object content) {
        this.type = type;
        this.state = state;
        this.message = message;
        this.content = content;
    }

    public static MessageInfo read(Intent intent) {

        MessageInfo messageInfo = (MessageInfo) intent.getSerializableExtra (FilterConstants.FilterParameterKey);

        return messageInfo;

    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "MessageInfo{" +
                "type=" + type +
                ", state=" + state +
                ", message='" + message + '\'' +
                ", content=" + content +
                '}';
    }

}
